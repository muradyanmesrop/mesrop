﻿
namespace HomeWork_Lesson25
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txFirstName = new System.Windows.Forms.TextBox();
            this.txLastName = new System.Windows.Forms.TextBox();
            this.txMiddleName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txAge = new System.Windows.Forms.TextBox();
            this.btInsert = new System.Windows.Forms.Button();
            this.btRename = new System.Windows.Forms.Button();
            this.btDelete = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btClean = new System.Windows.Forms.Button();
            this.comboBoxName = new System.Windows.Forms.ComboBox();
            this.txAgeMin = new System.Windows.Forms.TextBox();
            this.txAgeMax = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btFiltreAge = new System.Windows.Forms.Button();
            this.btResetParams = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(26, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Անուն";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(26, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ազգանուն";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(26, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "Հայրանուն";
            // 
            // txFirstName
            // 
            this.txFirstName.Location = new System.Drawing.Point(175, 37);
            this.txFirstName.Name = "txFirstName";
            this.txFirstName.Size = new System.Drawing.Size(221, 23);
            this.txFirstName.TabIndex = 3;
            this.txFirstName.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txFirstName_MouseClick);
            this.txFirstName.MouseEnter += new System.EventHandler(this.txFirstName_MouseEnter);
            this.txFirstName.MouseLeave += new System.EventHandler(this.txFirstName_MouseLeave);
            // 
            // txLastName
            // 
            this.txLastName.Location = new System.Drawing.Point(175, 78);
            this.txLastName.Name = "txLastName";
            this.txLastName.Size = new System.Drawing.Size(221, 23);
            this.txLastName.TabIndex = 4;
            this.txLastName.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txLastName_MouseClick);
            this.txLastName.MouseEnter += new System.EventHandler(this.txLastName_MouseEnter);
            this.txLastName.MouseLeave += new System.EventHandler(this.txLastName_MouseLeave);
            // 
            // txMiddleName
            // 
            this.txMiddleName.Location = new System.Drawing.Point(175, 121);
            this.txMiddleName.Name = "txMiddleName";
            this.txMiddleName.Size = new System.Drawing.Size(221, 23);
            this.txMiddleName.TabIndex = 5;
            this.txMiddleName.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txMiddleName_MouseClick);
            this.txMiddleName.MouseEnter += new System.EventHandler(this.txMiddleName_MouseEnter);
            this.txMiddleName.MouseLeave += new System.EventHandler(this.txMiddleName_MouseLeave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(26, 168);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 21);
            this.label4.TabIndex = 6;
            this.label4.Text = "Տարիք";
            // 
            // txAge
            // 
            this.txAge.Location = new System.Drawing.Point(175, 166);
            this.txAge.Name = "txAge";
            this.txAge.Size = new System.Drawing.Size(113, 23);
            this.txAge.TabIndex = 7;
            this.txAge.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txAge_MouseClick);
            this.txAge.MouseEnter += new System.EventHandler(this.txAge_MouseEnter);
            this.txAge.MouseLeave += new System.EventHandler(this.txAge_MouseLeave);
            // 
            // btInsert
            // 
            this.btInsert.Location = new System.Drawing.Point(26, 260);
            this.btInsert.Name = "btInsert";
            this.btInsert.Size = new System.Drawing.Size(75, 23);
            this.btInsert.TabIndex = 8;
            this.btInsert.Text = "Մուտք";
            this.btInsert.UseVisualStyleBackColor = true;
            this.btInsert.Click += new System.EventHandler(this.btInsert_Click);
            // 
            // btRename
            // 
            this.btRename.Location = new System.Drawing.Point(122, 260);
            this.btRename.Name = "btRename";
            this.btRename.Size = new System.Drawing.Size(75, 23);
            this.btRename.TabIndex = 9;
            this.btRename.Text = "Փոփոխել";
            this.btRename.UseVisualStyleBackColor = true;
            this.btRename.Click += new System.EventHandler(this.btInsert_Click);
            // 
            // btDelete
            // 
            this.btDelete.Enabled = false;
            this.btDelete.Location = new System.Drawing.Point(225, 260);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(75, 23);
            this.btDelete.TabIndex = 10;
            this.btDelete.Text = "Ջնջել";
            this.btDelete.UseVisualStyleBackColor = true;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(439, 37);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.Size = new System.Drawing.Size(514, 426);
            this.dataGridView1.TabIndex = 11;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // btClean
            // 
            this.btClean.Location = new System.Drawing.Point(358, 169);
            this.btClean.Name = "btClean";
            this.btClean.Size = new System.Drawing.Size(75, 23);
            this.btClean.TabIndex = 12;
            this.btClean.Text = "Մաքրել";
            this.btClean.UseVisualStyleBackColor = true;
            // 
            // comboBoxName
            // 
            this.comboBoxName.FormattingEnabled = true;
            this.comboBoxName.Location = new System.Drawing.Point(159, 328);
            this.comboBoxName.Name = "comboBoxName";
            this.comboBoxName.Size = new System.Drawing.Size(176, 23);
            this.comboBoxName.TabIndex = 13;
            this.comboBoxName.SelectedValueChanged += new System.EventHandler(this.comboBoxName_SelectedValueChanged);
            // 
            // txAgeMin
            // 
            this.txAgeMin.Location = new System.Drawing.Point(159, 381);
            this.txAgeMin.Name = "txAgeMin";
            this.txAgeMin.Size = new System.Drawing.Size(60, 23);
            this.txAgeMin.TabIndex = 14;
            this.txAgeMin.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txAgeMin_MouseClick);
            this.txAgeMin.MouseEnter += new System.EventHandler(this.txAgeMin_MouseEnter);
            this.txAgeMin.MouseLeave += new System.EventHandler(this.txAgeMin_MouseLeave);
            // 
            // txAgeMax
            // 
            this.txAgeMax.Location = new System.Drawing.Point(275, 381);
            this.txAgeMax.Name = "txAgeMax";
            this.txAgeMax.Size = new System.Drawing.Size(60, 23);
            this.txAgeMax.TabIndex = 15;
            this.txAgeMax.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txAgeMax_MouseClick);
            this.txAgeMax.MouseEnter += new System.EventHandler(this.txAgeMax_MouseEnter);
            this.txAgeMax.MouseLeave += new System.EventHandler(this.txAgeMax_MouseLeave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(225, 375);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 21);
            this.label5.TabIndex = 16;
            this.label5.Text = "____";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 381);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 15);
            this.label6.TabIndex = 17;
            this.label6.Text = "Զտել ըստ տարիքի";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 331);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 15);
            this.label7.TabIndex = 18;
            this.label7.Text = "Զտել ըստ անունի";
            // 
            // btFiltreAge
            // 
            this.btFiltreAge.Location = new System.Drawing.Point(341, 380);
            this.btFiltreAge.Name = "btFiltreAge";
            this.btFiltreAge.Size = new System.Drawing.Size(92, 23);
            this.btFiltreAge.TabIndex = 19;
            this.btFiltreAge.Text = "Հաստատել";
            this.btFiltreAge.UseVisualStyleBackColor = true;
            this.btFiltreAge.Click += new System.EventHandler(this.btFiltreAge_Click);
            // 
            // btResetParams
            // 
            this.btResetParams.Location = new System.Drawing.Point(27, 424);
            this.btResetParams.Name = "btResetParams";
            this.btResetParams.Size = new System.Drawing.Size(192, 23);
            this.btResetParams.TabIndex = 20;
            this.btResetParams.Text = "Զրոյացնել կարգավորումները";
            this.btResetParams.UseVisualStyleBackColor = true;
            this.btResetParams.Click += new System.EventHandler(this.btResetParams_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(964, 475);
            this.Controls.Add(this.btResetParams);
            this.Controls.Add(this.btFiltreAge);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txAgeMax);
            this.Controls.Add(this.txAgeMin);
            this.Controls.Add(this.comboBoxName);
            this.Controls.Add(this.btClean);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btDelete);
            this.Controls.Add(this.btRename);
            this.Controls.Add(this.btInsert);
            this.Controls.Add(this.txAge);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txMiddleName);
            this.Controls.Add(this.txLastName);
            this.Controls.Add(this.txFirstName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txFirstName;
        private System.Windows.Forms.TextBox txLastName;
        private System.Windows.Forms.TextBox txMiddleName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txAge;
        private System.Windows.Forms.Button btInsert;
        private System.Windows.Forms.Button btRename;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btClean;
        private System.Windows.Forms.ComboBox comboBoxName;
        private System.Windows.Forms.TextBox txAgeMin;
        private System.Windows.Forms.TextBox txAgeMax;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btFiltreAge;
        private System.Windows.Forms.Button btResetParams;
    }
}

