﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Linq;
using LinqConnectionToSQL;

namespace HomeWork_Lesson25
{
    public partial class Form1 : Form
    {
        int _id;
        DataClasses1DataContext db;

        bool[] Clickes = { false, false, false, false, false, false };
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GetRecord();
        }

        private void GetRecord()
        {
            db = new DataClasses1DataContext();
            dataGridView1.DataSource = db.Students;
            GetCombobox();
        }

        private void GetClean()
        {
            foreach (Control item in Controls)
            {
                if (item is TextBox)
                {
                    item.ResetText();
                    _id = 0;
                }
            }
        }

        private void btInsert_Click(object sender, EventArgs e)
        {
            
            if (IsValidTextBoxes())
            {
                if (_id == 0)
                {
                    Student student = new Student();
                    GetAddValues(student, "Your record is inserted: ");
                }
                else
                {
                    Student student = db.Students.FirstOrDefault(x => x.Id == _id);
                    GetAddValues(student, "Your record is updated: ");
                }
            }
            else
            {
                GetEmptyFields();
                MessageBox.Show("Please insert fileds: ", "Note", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void GetEmptyFields()
        {
            if (txFirstName.Text.Length == 0)
            {
                txFirstName.ForeColor = Color.Red;
                txFirstName.Text = "*    ";
            }
            if(txLastName.Text.Length == 0)
            {
                txLastName.ForeColor = Color.Red;
                txLastName.Text = "*    ";
            }
            if (txMiddleName.Text.Length == 0)
            {
                txMiddleName.ForeColor = Color.Red;
                txMiddleName.Text = "*    ";
            }
            if (txAge.Text.Length == 0)
            {
                txAge.ForeColor = Color.Red;
                txAge.Text = "*    ";
            }
        }
        public void GetAddValues(Student student, string message)
        {
            if(int.TryParse(txAge.Text, out int b))
            {
                student.fname = txFirstName.Text;
                student.lname = txLastName.Text;
                student.mname = txMiddleName.Text;
                student.age = int.Parse(txAge.Text);
                if (_id == 0)
                {
                    db.Students.InsertOnSubmit(student);
                }
                db.SubmitChanges();
                GetRecord();
                MessageBox.Show(message, "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                GetClean();
            }
            else
            {
                txAge.ForeColor = Color.Red;
                txAge.Text = "*    ";
                MessageBox.Show("Please enter correct age: ", "Not", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Student student = db.Students.SingleOrDefault(x => x.Id == _id);
                db.Students.DeleteOnSubmit(student);
                db.SubmitChanges();
                GetRecord();
                GetClean();
                MessageBox.Show("Your record is deleted: ", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Please choose fields: ", "Note", MessageBoxButtons.OK, MessageBoxIcon.Error);
                GetEmptyFields();
            }
        }

        private bool IsValidTextBoxes()
        {
            if (txFirstName.Text.Length == 0 || txLastName.Text.Length == 0 || txMiddleName.Text.Length == 0 || txAge.Text.Length == 0)
            {
                return false;
            }
            return true;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                txFirstName.ForeColor = Color.Black;
                txLastName.ForeColor = Color.Black;
                txMiddleName.ForeColor = Color.Black;
                txAge.ForeColor = Color.Black;
                txFirstName.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                txLastName.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
                txMiddleName.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
                txAge.Text = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();

                _id = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value);
                btDelete.Enabled = true;
            }
            else
            {
                GetClean();
            }
        }
        private void GetMouse(TextBox txvalue, string text)
        {
            if (txvalue.Text.Length == 0)
            {
                txvalue.ForeColor = Color.Gray;
                txvalue.Text = $"{text}      ";
            }
        }
        private void GetMouseReset(TextBox textBox, string text, string text2 = null)
        {
            if (textBox.Text == $"{text}      " || textBox.Text == text2)
            {
                textBox.ResetText();
                textBox.ForeColor = Color.Black;
            }
        }
        private void btFiltreAge_Click(object sender, EventArgs e)
        {
            try
            {
                if (txAgeMin.Text.Length > 0 && txAgeMax.Text.Length > 0)
                {
                    dataGridView1.DataSource = db.Students.Where(x => x.age >= int.Parse(txAgeMin.Text) && x.age <= int.Parse(txAgeMax.Text));
                    MessageBox.Show("Your request has been filtered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if(txAgeMin.Text.Length > 0)
                {
                    dataGridView1.DataSource = db.Students.Where(x => x.age >= int.Parse(txAgeMin.Text));
                    MessageBox.Show("Your request has been filtered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    dataGridView1.DataSource = db.Students.Where(x => x.age <= int.Parse(txAgeMax.Text));
                    MessageBox.Show("Your request has been filtered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch
            {
                MessageBox.Show("Please enter correct fields: ", "Not", MessageBoxButtons.OK, MessageBoxIcon.Error);
                GetRecord();
            }            
        }
        private void btResetParams_Click(object sender, EventArgs e)
        {
            GetRecord();
            MessageBox.Show("Parames has been resets", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #region Mouse Enter, Leave and Click
        private void txFirstName_MouseEnter(object sender, EventArgs e)
        {
            GetMouse(txFirstName, "Լրացնել անունը");
        }

        private void txFirstName_MouseLeave(object sender, EventArgs e)
        {
            GetMouseReset(txFirstName, "Լրացնել անունը");
        }

        private void txFirstName_MouseClick(object sender, MouseEventArgs e)
        {
            ResetMouseEnter();
            GetMouseReset(txFirstName, "Լրացնել անունը", "*    ");
            txFirstName.MouseEnter -= txFirstName_MouseEnter;
            Clickes[0] = true;
        }

        private void txLastName_MouseEnter(object sender, EventArgs e)
        {
            GetMouse(txLastName, "Լրացնել ազգանունը");
        }

        private void txLastName_MouseLeave(object sender, EventArgs e)
        {
            GetMouseReset(txLastName, "Լրացնել ազգանունը");
        }

        private void txLastName_MouseClick(object sender, MouseEventArgs e)
        {
            ResetMouseEnter();
            GetMouseReset(txLastName, "Լրացնել ազգանունը", "*    ");
            txLastName.MouseEnter -= txLastName_MouseEnter;
            Clickes[1] = true;
        }

        private void txMiddleName_MouseEnter(object sender, EventArgs e)
        {
            GetMouse(txMiddleName, "Լրացնել հայրանունը");
        }

        private void txMiddleName_MouseLeave(object sender, EventArgs e)
        {
            GetMouseReset(txMiddleName, "Լրացնել հայրանունը");
        }

        private void txMiddleName_MouseClick(object sender, MouseEventArgs e)
        {
            GetMouseReset(txMiddleName, "Լրացնել հայրանունը", "*    ");
            ResetMouseEnter();
            txMiddleName.MouseEnter -= txMiddleName_MouseEnter;
            Clickes[2] = true;
        }

        private void txAge_MouseEnter(object sender, EventArgs e)
        {
            GetMouse(txAge, "Լրացնել տարիքը");
        }

        private void txAge_MouseLeave(object sender, EventArgs e)
        {
            GetMouseReset(txAge, "Լրացնել տարիքը");
        }

        private void txAge_MouseClick(object sender, MouseEventArgs e)
        {
            GetMouseReset(txAge, "Լրացնել տարիքը", "*    ");
            ResetMouseEnter();
            txAge.MouseEnter -= txAge_MouseEnter;
            Clickes[3] = true;
        }

        private void txAgeMin_MouseEnter(object sender, EventArgs e)
        {
            GetMouse(txAgeMin, "Min");
        }

        private void txAgeMin_MouseLeave(object sender, EventArgs e)
        {
            GetMouseReset(txAgeMin, "Min");
        }

        private void txAgeMin_MouseClick(object sender, MouseEventArgs e)
        {
            GetMouseReset(txAgeMin, "Min");
            ResetMouseEnter();
            txAgeMin.MouseEnter -= txAgeMin_MouseEnter;
            Clickes[4] = true;
        }

        private void txAgeMax_MouseEnter(object sender, EventArgs e)
        {
            GetMouse(txAgeMax, "Max");
        }

        private void txAgeMax_MouseLeave(object sender, EventArgs e)
        {
            GetMouseReset(txAgeMax, "Max");
        }

        private void txAgeMax_MouseClick(object sender, MouseEventArgs e)
        {
            GetMouseReset(txAgeMax, "Max");
            ResetMouseEnter();
            txAgeMax.MouseEnter -= txAgeMax_MouseEnter;
            Clickes[5] = true;
        }
        private void ResetMouseEnter()
        {
            if (Clickes[0]) { txFirstName.MouseEnter += txFirstName_MouseEnter; Clickes[0] = false; }
            else if (Clickes[1]) { txLastName.MouseEnter += txLastName_MouseEnter; Clickes[1] = false; }
            else if (Clickes[2]) { txMiddleName.MouseEnter += txMiddleName_MouseEnter; Clickes[2] = false; }
            else if (Clickes[3]) { txAge.MouseEnter += txAge_MouseEnter; Clickes[3] = false; }
            else if (Clickes[4]) { txAgeMin.MouseEnter += txAgeMin_MouseEnter; Clickes[4] = false; }
            else if (Clickes[5]) { txAgeMax.MouseEnter += txAgeMax_MouseEnter; Clickes[5] = false; }


        }
        #endregion
        private void GetCombobox()
        {
            comboBoxName.Items.Clear();
            HashSet<string> hs = new HashSet<string>();
            foreach (var item in db.Students.Select(x => x.fname))
            {
                hs.Add(item);
            }
            comboBoxName.Items.AddRange(hs.ToArray());
        }

        private void comboBoxName_SelectedValueChanged(object sender, EventArgs e)
        {
            dataGridView1.DataSource = db.Students.Where(x => x.fname == comboBoxName.SelectedItem.ToString());
        }
    }
}
